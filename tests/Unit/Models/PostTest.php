<?php

namespace Tests\Unit\Models;

use App\DTO\Collection\TagCollection;
use App\DTO\PostDto;
use App\DTO\TagDto;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class PostTest extends TestCase
{
    use DatabaseMigrations;

    public function testPersistMethodShouldValidateDataAndPersistPostAndReturnPostInstance()
    {
        $this->runDatabaseMigrations();
        $this->seed();

        // test persist functionality
        $tags = Tag::all()->random(2);

        $category = Category::inRandomOrder()->first();

        $sentence = Factory::create()->paragraph(rand(3, 7), true);
        $post = Post::create([
            'title'=>'Testing Post',
            'excerpt'=>Factory::create()->sentence(rand(10, 10)),
            'content'=>$sentence,
            'image'=>'posts/1.jpg',
            'category_id'=>$category->id,
            'user_id'=>2,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);
        $post->tags()->attach($tags);

        $this->assertNotNull($post);
        $this->assertInstanceOf(Post::class, $post);
        $this->assertDatabaseHas("Posts", ["title" => "Testing Post"]);
        // 5 will be created in DB
        $this->assertDatabaseCount("Posts", 6);
        $this->assertEquals($sentence, $post->content);


        $tagCollection = new TagCollection();
        foreach ($tags as $tag){
            $tagCollection->push(TagDto::fromModel($tag));
        }
        // Persisting through custom persist method

        $this->expectException(ValidationException::class);
        // Persist post should through validation exception for null image
        Post::persistPost(new PostDto(
            id: 0,
            category_id: $category->id,
            user_id: 2,
            collection_of_tags: $tagCollection,
            title: "Testing Post 2",
            excerpt: Factory::create()->sentence(rand(10, 10)),
            content: $sentence,
            imageFile: null,
            published_at: Carbon::now()->format('Y-m-d'),
        ));
    }
}
