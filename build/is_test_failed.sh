parent_path=$(dirname "$0")
test_passed=$(cat "$parent_path/../tests/test_report.txt" | grep OK)

if [ -z "${test_passed}" ]; then
    echo "1"
else
    echo "0"
fi
