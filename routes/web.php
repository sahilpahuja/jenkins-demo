<?php

use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\PostsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class, 'index'])->name('welcome');
Route::get('/blog/{post}', [WelcomeController::class, 'show'])->name('blog.show');
Route::get('/blog/categort/{category}', [WelcomeController::class, 'category'])->name('blog.category');
Route::get('/blog/tag/{tag}', [WelcomeController::class, 'tag'])->name('blog.tag');

Auth::routes();
Route::middleware(['auth'])->group(function (){
    Route::get('/home', [HomeController::class, 'index'])->name('home');

//This will create routes for 7 default actions automatically.
    Route::resource('categories', CategoriesController::class);
    Route::resource('tags', TagsController::class);
    Route::resource('posts', PostsController::class);
    Route::delete('/trash/{post}', [PostsController::class, 'trash'])->name('posts.trash');
    Route::get('/trashed', [PostsController::class, 'trashed'])->name('posts.trashed');
    Route::put('/restore/{post}', [PostsController::class, 'restore'])->name('posts.restore');
});
Route::middleware(['auth', 'admin'])->group(function() {
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    Route::put('/users/{user}/make-admin', [UsersController::class, 'makeAdmin'])->name('users.make-admin');
});
