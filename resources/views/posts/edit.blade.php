@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Edit Post</div>

        <div class="card-body">
            <form action="{{ route('posts.update', $postDto->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                           value="{{ old('title', $postDto->title) }}"
                           class="form-control @error('title') is-invalid @enderror"
                           name="title" id="title">
                    @error('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="category_id">Category</label>
                    <select name="category_id" id="category_id" class="form-control select2">
                        <option value="" selected disabled>SELECT</option>
                        @foreach($categoyCollection as $categoryDto)
                            <option value="{{ $categoryDto->id }}" @if($postDto->category_id == $categoryDto->id) selected @endif>{{ $categoryDto->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="tags">Tags</label>
                    <select name="tags[]" id="tags" class="form-control select2" multiple>
                        @foreach($tagCollection as $tagDto)
                            <option value="{{ $tagDto->id }}"
                                {{ old('tags') ? (in_array($tagDto->id, old('tags')) ?'selected' : '') : (in_array($tagDto->id, $postDto->collection_of_tags->pluck('id')->toArray()) ? 'selected' : '') }}>{{ $tagDto->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="excerpt">Excerpt</label>
                    <textarea name="excerpt"
                              id="excerpt"
                              class="form-control @error('excerpt') is-invalid @enderror"
                              rows="4">{{ old('excerpt', $postDto->excerpt) }}</textarea>
                    @error('excerpt')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <input type="hidden" id="content" name="content" value="{{ old('content', $postDto->content) }}">
                    <trix-editor input="content"></trix-editor>

                    @error('content')@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
@endsection

                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="published_at">Published At</label>
                    <input type="text"
                           value="{{ old('published_at', $postDto->published_at) }}"
                           class="form-control"
                           name="published_at" id="published_at">
                    @error('published_at')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <img src="{{ asset('storage/'.$postDto->image) }}" alt="">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file"
                           class="form-control @error('image') is-invalid @enderror"
                           name="image" id="image">
                    @error('image')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Update Post</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
@endsection
