<!-- JQuery Core
=====================================-->
<script src="{{ asset('assets/js/core/bootstrap-3.3.7.min.js') }}"></script>

<!-- Magnific Popup
=====================================-->
<script src="{{ asset('assets/js/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/js/magnific-popup/magnific-popup-zoom-gallery.js') }}"></script>

<!-- JQuery Main
=====================================-->
<script src="{{ asset('assets/js/main/jquery.appear.js') }}"></script>
<script src="{{ asset('assets/js/main/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/js/main/parallax.min.js') }}"></script>
<script src="{{ asset('assets/js/main/jquery.sticky.js') }}"></script>
<script src="{{ asset('assets/js/main/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/js/main/main.js') }}"></script>