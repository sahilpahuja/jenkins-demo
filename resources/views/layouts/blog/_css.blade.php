<!-- Load Core CSS
    =====================================-->
    <link rel="stylesheet" href="{{ asset('assets/css/core/animate.min.css') }}">

    <!-- Load Main CSS
    =====================================-->
    <link rel="stylesheet" href="{{ asset('assets/css/main/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main/setting.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main/hover.css') }}">


    <link rel="stylesheet" href="{{ asset('assets/css/color/pasific.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/icon/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/icon/et-line-font.css') }}">