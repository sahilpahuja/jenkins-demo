const title=$("#title"), tags = $("#tags"), content=$("#content");

const previewTitle=$("#preview-title"), previewImage = $('#img-preview'), previewContent=$("#preview-content"), previewTagParent = $('#preview-tag-parent');

function readURL(input) {
    if (input.files && input.files[0]) {   
        previewImage.attr('src', URL.createObjectURL(input.files[0]));
    }
}

$('#preview-btn').on('click', function() {
    
    previewTitle.text(title.val()==''?'N.A':title.val());
    previewContent.html(content.val()==''?'N.A':content.val());

    let tagsArr = tags.find(":selected").toArray();
    if(tagsArr.length > 0){
        previewTagParent.text('');
        tagsArr.forEach(function(element, ind) {
            if(!(tagsArr.length-1 == ind)){
                previewTagParent.append(`<a href="#">${$(element).text()}</a>, `);
            }else{
                previewTagParent.append(`<a href="#">${$(element).text()}</a>`);
            }
        });
    }else{
        previewTagParent.text('N.A');
    }
    
});