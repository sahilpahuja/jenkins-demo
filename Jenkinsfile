pipeline {
    agent any

    environment {
        SSH_CREDENTIALS_ID = 'free-server-ssh-credentials'
        REMOTE_USER = 'ubuntu'
        REMOTE_HOST = '13.201.57.90'
        REMOTE_PATH = '/home/ubuntu/repos/jenkins-demo'
    }

    stages {
        stage("Git Pull") {
            steps {
                sshagent([SSH_CREDENTIALS_ID]) {
                    sh """
                        ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${REMOTE_HOST} << EOF
                            cd ${REMOTE_PATH}
                            git status
                            git fetch
                            git pull origin main
                            exit
                        EOF
                    """
                }
            }
        }
        stage("Deploy") {
            steps {
                sshagent([SSH_CREDENTIALS_ID]) {
                    sh """
                        ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${REMOTE_HOST} << EOF
                            cd ${REMOTE_PATH}
                            composer install
                            php artisan migrate
                            php artisan config:clear
                            exit
                        EOF
                    """
                }
            }
        }
        stage("Run Tests") {
            steps {
                script {
                    sshagent([SSH_CREDENTIALS_ID]) {
                        sh """
                            ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${REMOTE_HOST} << EOF
                                cd ${REMOTE_PATH}
                                ./vendor/bin/phpunit > ./tests/test_report.txt
                                cat ./tests/test_report.txt
                                exit
                            EOF
                        """
                    }
                }
            }
        }
        stage("Check Test") {
            steps {
                script {
                    sshagent([SSH_CREDENTIALS_ID]) {
                        def test_result = sh(script: """
                            ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${REMOTE_HOST} "${REMOTE_PATH}/build/is_test_failed.sh"
                        """, returnStdout: true).trim()

                        if(test_result.toInteger() > 0) {
                            println("Your test[s] has been failed")
                            error "Unit Test is failed"
                        }
                    }
                }
            }
        }
    }
}