<?php


namespace App\Services;


use App\DTO\Casters\CategoryCollectionCaster;
use App\DTO\Casters\TagCollectionCaster;
use App\DTO\PostDto;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;

class PostService
{
    /**
     * @var TagCollectionCaster
     */
    private TagCollectionCaster $tagCollectionCaster;
    /**
     * @var CategoryCollectionCaster
     */
    private CategoryCollectionCaster $categoryCollectionCaster;

    /**
     * PostService constructor.
     */
    public function __construct()
    {
        $this->tagCollectionCaster =  new TagCollectionCaster();
        $this->categoryCollectionCaster =  new CategoryCollectionCaster();

    }

    public function index($isAdmin)
    {
        $posts = null;
        if(!$isAdmin) {
            $posts = Post::withoutTrashed()->where('user_id', auth()->id())->paginate(10);
        } else {
            $posts = Post::paginate(10);
        }
        return ['posts' => $posts];
    }
    public function getPostBindings()
    {
        $categoyCollection = $this->categoryCollectionCaster->cast(Category::all()->toArray());
        $tagCollection = $this->tagCollectionCaster->cast(Tag::all()->toArray());
        return compact(['categoyCollection', 'tagCollection']);
    }
    public function create(PostDto $postDto)
    {
        try {
            return Post::persistPost($postDto);
        }catch (Throwable $e) {
            throw $e;
        }
    }

    public function editData(Post $post)
    {
        return array_merge($this->getPostBindings(), ['postDto' => PostDto::fromModel($post)]);
    }

    public function update(Post $post, PostDto $postDto)
    {
        try {
            return $post->updatePost($postDto);
        }catch (Throwable $e) {
            throw $e;
        }
    }

    public function destroy(int $id)
    {
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->deleteImage();
        $post->forceDelete();
    }

    public function trash(Post $post)
    {
        $post->delete();
    }

    public function trashed()
    {
        return Post::onlyTrashed()->paginate(10);
    }

    public function restore(int $id)
    {
        $trashedPost = Post::onlyTrashed()->findOrFail($id);
        $trashedPost->restore();
    }
}
