<?php


namespace App\Services;


use App\DTO\Casters\TagCollectionCaster;
use App\DTO\Collection\TagCollection;
use App\DTO\TagDto;
use App\Models\Tag;
use Throwable;

class TagService
{
    public function __construct()
    {
        $this->tagCollectionCaster =  new TagCollectionCaster();

    }
    public function index()
    {
        $tagCollection = new TagCollection();
        foreach (Tag::all() as $tag){
            $tagData = $tag->toArray();
            $tagData['postCount'] = $tag->posts()->count();
            $tagCollection->push(new TagDto($tagData));
        }
        return compact('tagCollection');
    }

    public function create(TagDto $tagDto)
    {
        try {
            return Tag::persistTag($tagDto);
        }catch (Throwable $e) {
            throw $e;
        }
    }

    public function editData(Tag $tag)
    {
        return ['tagDto' => TagDto::fromModel($tag)];
    }
    public function update(Tag $tag, TagDto $tagDto)
    {
        if ($tag->name !== $tagDto->name) {
            try {
                return $tag->updateTag($tagDto);
            }catch (Throwable $e) {
                throw $e;
            }
        } else {
            return false;
        }
    }

    public function delete(Tag $tag)
    {
        if($tag->posts->count()>0) {
            return false;
        }
        $tag->delete();
        return true;
    }
}
