<?php


namespace App\Services;


use App\DTO\Casters\CategoryCollectionCaster;
use App\DTO\CategoryDto;
use App\Models\Category;

class CategoryService
{
    /**
     * @var CategoryCollectionCaster
     */
    private CategoryCollectionCaster $categoryCollectionCaster;

    public function __construct()
    {
        $this->categoryCollectionCaster =  new CategoryCollectionCaster();

    }
    public function index()
    {
        $categoyCollection = $this->categoryCollectionCaster->cast(Category::all()->toArray());
        return compact('categoyCollection');
    }

    public function create(CategoryDto $categoryDto)
    {
        try {
            return Category::persistCategory($categoryDto);
        }catch (Throwable $e) {
            throw $e;
        }
    }

    public function editData(Category $category)
    {
        return ['categoryDto' => CategoryDto::fromModel($category)];
    }

    public function update(Category $category, CategoryDto $categoryDto)
    {
        if ($category->name !== $categoryDto->name) {
            try {
                return $category->updateCategory($categoryDto);
            }catch (Throwable $e) {
                throw $e;
            }
        } else {
            return false;
        }
    }

    public function delete(Category $category)
    {
        if($category->posts->count()>0)
        {
            return false;
        }
        $category->delete();
        return true;
    }
}
