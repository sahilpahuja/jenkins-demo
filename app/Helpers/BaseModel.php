<?php

namespace App\Helpers;


use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    protected $guarded = ['id'];

    abstract public static function getCreateValidationRules(): array;
    abstract public static function getUpdateValidationRules(): array;

}
