<?php

namespace App\Models;

use App\DTO\CategoryDto;
use App\Helpers\BaseModel;
use App\Helpers\Utils;
use App\Interfaces\CategoryConstants;

class Category extends BaseModel implements CategoryConstants
{
    protected $fillable = ['name'];
    public static function persistCategory(CategoryDto $categoryDto): Category
    {
        //Validation
        Utils::validateOrThrow(self::CREATE_RULES, ['name'=>$categoryDto->name]);

        return Category::create($categoryDto->except('id')->toArray());
    }

    public function updateCategory(CategoryDto $categoryDto): self
    {
        $this->update($categoryDto->except('id')->toArray());
        return $this;
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public static function getCreateValidationRules(): array
    {
        return self::CREATE_RULES;
    }

    public static function getUpdateValidationRules(): array
    {
        return self::UPDATE_RULES;
    }
}
