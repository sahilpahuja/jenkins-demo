<?php

namespace App\Models;

use App\DTO\TagDto;
use App\Helpers\BaseModel;
use App\Helpers\Utils;
use App\Interfaces\TagConstants;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Tag extends BaseModel implements TagConstants
{
    protected $fillable = ['name'];

    public static function persistTag(TagDto $tagDto): Tag
    {
        //Validation
        Utils::validateOrThrow(self::CREATE_RULES, ['name'=>$tagDto->name]);

        return Tag::create($tagDto->except('id')->toArray());
    }

    public function updateTag(TagDto $tagDto) : self
    {
        $this->update($tagDto->except('id')->toArray());
        return $this;
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class)->withTimestamps();
    }

    public static function getCreateValidationRules(): array
    {
        return self::CREATE_RULES;
    }

    public static function getUpdateValidationRules(): array
    {
        return self::UPDATE_RULES;
    }
}
