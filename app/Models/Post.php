<?php

namespace App\Models;

use App\DTO\PostDto;
use App\Helpers\BaseModel;
use App\Helpers\Utils;
use App\Interfaces\PostConstants;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class Post extends BaseModel implements PostConstants
{
    use SoftDeletes;

    protected $dates = ['published_at'];
    protected $fillable = [
        'title',
        'category_id',
        'excerpt',
        'content',
        'image',
        'published_at',
        'user_id'
    ];

    /**
     * @param int $userId
     * @param int $categoryId
     * @param PostDto $postDto
     * @param array $tags
     * @return Post
     * @throws \Throwable
     *
     */
    public static function persistPost(PostDto $postDto): Post
    {
        //Validation
        Utils::validateOrThrow(array_diff_key(self::CREATE_RULES, ['image'=>'']), $postDto->toArray());

        //Creation
        $post = null;
        DB::transaction(function () use ($postDto, &$post) {
            if ($postDto->imageFile === null)
            {
                throw ValidationException::withMessages([
                    'image' => ['image field is required']
                ]);
            }
            $postDto->image = $postDto->imageFile->store('posts');

            $post = Post::create($postDto->except('id', 'tags')->toArray());
            $post->tags()->attach($postDto->collection_of_tags->pluck('id')->toArray());
        });
        return $post;
    }

    public function updatePost(PostDto $postDto): self
    {
        //Validation


        //Creation
        DB::transaction(function () use ($postDto) {
            $data = $postDto->except('id', 'tags')->toArray();
            if($postDto->imageFile !== null){
                $data['image'] = $postDto->imageFile->store('posts');
            } else {
                unset($data['image']);
            }
            $this->update($data);
            $this->tags()->sync($postDto->collection_of_tags->pluck('id')->toArray());
        });
        return $this;
    }

    /*
     * HELPER FUNCTIONS
     */
    public function deleteImage()
    {
        Storage::delete($this->image);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();

    }
    public function hasTag($tag_id)
    {
        return in_array($tag_id, $this->tags->pluck('id')->toArray());
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * QUERY SCOPES
     */

     public function scopePublished($query) {
         return $query->where('published_at', '<=', now());
     }

     public function scopeSearch($query) {
         $search = request('search');
         if($search) {
             return $query->where('title', 'like', "%$search%");
         }
         return $query;
     }

    public static function getCreateValidationRules(): array
    {
        return self::CREATE_RULES;
    }

    public static function getUpdateValidationRules(): array
    {
        return self::UPDATE_RULES;
    }
}
