<?php


namespace App\Interfaces;


interface PostConstants
{
    const CREATE_RULES = [
        'title'=>'required|unique:posts',
        'excerpt'=>'required|max:255',
        'content'=>'required',
        'image'=>'required'
    ];
//    const UPDATE_RULES = ['name'=> 'required|min:10'];
}
