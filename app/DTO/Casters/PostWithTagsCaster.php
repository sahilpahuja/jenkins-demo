<?php


namespace App\DTO\Casters;


use App\DTO\Collection\PostCollection;
use App\DTO\PostDto;

class PostWithTagsCaster
{
    public function cast(mixed $value): PostCollection
    {
        $tagCaster = new TagCollectionCaster();
        return new PostCollection(array_map(function (array $data) use ($tagCaster) {
            $data['collection_of_tags'] = $tagCaster->cast($data['tags']);
            return new PostDto(...$data);
        }, $value));
    }
}
