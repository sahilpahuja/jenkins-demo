<?php


namespace App\DTO\Casters;


use App\DTO\Collection\TagCollection;
use App\DTO\TagDto;
use Spatie\DataTransferObject\Caster;

class TagCollectionCaster implements Caster
{
    public function cast(mixed $value): TagCollection
    {
        return new TagCollection(array_map(fn (array $data) => new TagDto(...$data), $value));
    }
}
