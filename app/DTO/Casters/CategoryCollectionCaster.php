<?php


namespace App\DTO\Casters;


use App\DTO\CategoryDto;
use App\DTO\Collection\CategoryCollection;
use Spatie\DataTransferObject\Caster;

class CategoryCollectionCaster implements Caster
{
    public function cast(mixed $value): CategoryCollection
    {
        return new CategoryCollection(array_map(fn (array $data) => new CategoryDto(...$data), $value));
    }
}
