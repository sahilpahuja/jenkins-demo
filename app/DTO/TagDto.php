<?php


namespace App\DTO;

use App\Models\Tag;
use Spatie\DataTransferObject\DataTransferObject;

class TagDto extends DataTransferObject
{
    public int $id;
    public ?int $postCount;
    public string $name;

    public static function fromModel(Tag $tag): TagDto
    {
        return new TagDto(
            id: $tag->id,
            name: $tag->name,
        );
    }
}
