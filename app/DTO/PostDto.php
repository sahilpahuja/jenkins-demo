<?php


namespace App\DTO;


use App\ArrayCaster\TagsArrayCaster;
use App\DTO\Casters\TagCollectionCaster;
use App\DTO\Collection\TagCollection;
use App\Models\Post;
use Illuminate\Http\UploadedFile;
use Spatie\DataTransferObject\DataTransferObject;
use function Spatie\DataTransferObject\Casters\cast;

class PostDto extends DataTransferObject
{
    #[NumberBetween(1, 100)]
    public int $id;
    public int $category_id;
    public int $user_id;
    public TagCollection $collection_of_tags;
    public string $title;
    public ?string $image;
    public string $excerpt;
    public string $content;
    public string $published_at;
    public ?UploadedFile $imageFile;

    /**
     * @param Post $post
     * @return PostDto
     */
    public static function fromModel(Post $post): PostDto
    {
        return new PostDto(
            id: $post->id,
            category_id: $post->category_id,
            user_id: $post->user_id,
            collection_of_tags: (new TagCollectionCaster())->cast($post->tags->toArray()),
            title: $post->title,
            image: $post->image,
            excerpt: $post->excerpt,
            content: $post->content,
            published_at: $post->published_at,
        );
    }
}
