<?php


namespace App\DTO;

use App\Models\Category;
use Spatie\DataTransferObject\DataTransferObject;

class CategoryDto extends DataTransferObject
{
    public int $id;
    public string $name;

    public static function fromModel(Category $category): CategoryDto
    {
        return new CategoryDto(
            id: $category->id,
            name: $category->name,
        );
    }
}
