<?php


namespace App\DTO;


use App\DTO\Casters\PostWithTagsCaster;
use App\DTO\Casters\TagCollectionCaster;
use App\Models\Post;
use App\Models\Tag;

class Demo
{
    public function testTags()
    {
        $tags = Tag::all();
        $caster = new TagCollectionCaster();
        $tagCollection = $caster->cast($tags->toArray());

        return $tagCollection;


//        $posts = Post::with('tags')->get();
//        dd($posts->items());
//        $caster = new PostWithTagCaster();
//        $collectionOfPosts = $caster->cast($posts->items());
        return $collectionOfPosts;
    }

    public function test()
    {
        $post = Post::with('tags')->get();
        $caster = new PostWithTagsCaster();
        $postCollection = $caster->cast($post->toArray());

        return $postCollection->get(0)->collection_of_tags->pluck('id');
    }
}
