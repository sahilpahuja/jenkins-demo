<?php

namespace App\Http\Requests\Post;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class CreatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $createRules = array_diff_key(Post::getCreateValidationRules(), ['image'=>'']);
        return array_merge($createRules, [
            'image' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:1024',
            'category_id'=>'required|exists:categories,id',
        ]);
    }
}
