<?php

namespace App\Http\Controllers;

use App\DTO\BaseDTO;
use App\DTO\Casters\TagCollectionCaster;
use App\DTO\Collection\TagCollection;
use App\DTO\PostDto;
use App\DTO\TagDto;
use App\Models\Category;
use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Models\Post;
use App\Models\Tag;
use App\Services\PostService;

class PostsController extends Controller
{
    private PostService $postService;
    public function __construct(PostService $ref)
    {
        $this->postService = $ref;
        $this->middleware(['verifyCategoriesCount'])->only('create', 'store');
        $this->middleware(['validateAuthor'])->only('edit', 'update', 'destroy', 'trash');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->postService->index(auth()->user()->isAdmin());
        return view('posts.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->postService->getPostBindings();
        return view('posts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        //Image Upload and stores the name of image
        // run command: php artisan storage:link
        $image = $request->file('image');

        $postDto = new PostDto(
            id: 0,
            category_id: $request->category_id,
            user_id: auth()->id(),
            collection_of_tags: $this->getTagCollection($request->tags),
            title: $request->title,
            excerpt: $request->excerpt,
            content: $request->content,
            imageFile: $image,
            published_at: $request->published_at,
        );

        try {
            $this->postService->create($postDto);
        } catch (\Throwable $e){
            session()->flash('error', 'Failed to add Post!');
            return redirect(route('posts.index'));
        }
        session()->flash('success', 'Post Created Successfully!');
        return redirect(route('posts.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $data = $this->postService->editData($post);
        return view('posts.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $image = $request->file('image');

        $postDto = new PostDto(
            id: 0,
            category_id: $request->category_id,
            user_id: auth()->id(),
            collection_of_tags: $this->getTagCollection($request->tags),
            title: $request->title,
            excerpt: $request->excerpt,
            content: $request->content,
            imageFile: $image,
            published_at: $request->published_at,
        );

        try{
            $this->postService->update($post, $postDto);
        } catch (\Throwable $e){
            session()->flash('error', 'Failed to update Post!');
            return redirect(route('posts.index'));
        }
        session()->flash('success', 'Post Updated Successfully!');
        return redirect(route('posts.index'));
    }

    private function getTagCollection($tags)
    {
        $tagCollection = new TagCollection();
        foreach ($tags as $tagId){
            $tagCollection->push(TagDto::fromModel(Tag::find($tagId)));
        }
        return $tagCollection;
    }

    /**
     * Remove the specified resource from storage permanently.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->postService->destroy($id);
        session()->flash('success', 'Post Deleted Successfully!');
        return redirect()->back();
    }

    /*
     * Soft deletes the Post
     */
    public function trash(Post $post)
    {
        $this->postService->trash($post);
        session()->flash('success', 'Post trashed successfully!');
        return redirect(route('posts.index'));
    }
    public function trashed()
    {
        $trashed = $this->postService->trashed();
        return view('posts.trashed')->with('posts', $trashed);
    }

    public function restore($id)
    {
        $this->postService->restore($id);
        session()->flash('success', 'Post restored successfully!');
        return redirect(route('posts.index'));
    }
}
