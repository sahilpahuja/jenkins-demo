<?php

namespace App\Http\Controllers;

use App\DTO\CategoryDto;
use App\Models\Category;
use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    private CategoryService $categoryService;
    /**
     * CategoriesController constructor.
     */
    public function __construct(CategoryService $ref)
    {
        $this->categoryService = $ref;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->categoryService->index();
        return view('categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        try {
            $this->categoryService->create(new CategoryDto(
                id: 0,
                name: $request->name
            ));
        } catch (\Throwable $e){
            session()->flash('error', 'Failed to add Category!');
            return redirect(route('categories.index'));
        }
        session()->flash('success', 'Category Added Successfully');
        return redirect(route('categories.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $data = $this->categoryService->editData($category);
        return view('categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        try {
            if($this->categoryService->update($category, new CategoryDto(id: $category->id, name: $request->name)) === false)
            {
                session()->flash('success', 'No changes made!');
            } else {
                session()->flash('success', 'Category Updated Successful!');
            }
        } catch (\Throwable $e){
            session()->flash('error', 'Failed to update Category!');
            return redirect(route('categories.index'));
        }
        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if(!($this->categoryService->delete($category))) {
            session()->flash('error', 'Category cannot be deleted as it is associated with some post!');
            return redirect()->back();
        }
        session()->flash('success', 'Category Deleted Successfully!');
        return redirect(route('categories.index'));
    }
}
