<?php

namespace App\Http\Controllers;

use App\DTO\TagDto;
use App\Http\Requests\Tag\CreateTagRequest;
use App\Http\Requests\Tag\UpdateTagRequest;
use App\Models\Tag;
use App\Services\TagService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    private TagService $tagService;
    /**
     * CategoriesController constructor.
     */
    public function __construct(TagService $ref)
    {
        $this->tagService = $ref;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->tagService->index();
        return view('tags.index', $data);
    }
    public function create()
    {
        return view('tags.create');
    }
    public function store(CreateTagRequest $request)
    {
        try {
            $this->tagService->create(new TagDto(
                id: 0,
                name: $request->name
            ));
        } catch (\Throwable $e){
            session()->flash('error', 'Failed to add Tag!');
            return redirect(route('tags.index'));
        }
        session()->flash('success', 'Tag Added Successfully');
        return redirect(route('tags.index'));
    }
    public function edit(Tag $tag)
    {
        $data = $this->tagService->editData($tag);
        return view('tags.edit', $data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        try {
            if($this->tagService->update($tag, new TagDto(id: $tag->id, name: $request->name)) === false)
            {
                session()->flash('success', 'No changes made!');
            } else {
                session()->flash('success', 'Tag Updated Successful!');
            }
        } catch (\Throwable $e){
            session()->flash('error', 'Failed to update Tag!');
            return redirect(route('tags.index'));
        }

        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if(!($this->tagService->delete($tag))) {
            session()->flash('error', 'Tag cannot be deleted as it is associated with some post!');
            return redirect()->back();
        }
        session()->flash('success', 'Tag Deleted Successfully!');
        return redirect(route('tags.index'));
    }
}
